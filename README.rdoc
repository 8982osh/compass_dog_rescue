=== README

Purpose: To promote the adoption of dogs rescued from South Texas and transported to the Detroit area.

Ruby: 2.2.1

Bootstrap-sass: 3.3.7

Heroku: https://safe-springs-36955.herokuapp.com/

Bitbucket: https://bitbucket.org/8982osh/compass

Email testing at Mailtrap.io

Login page is for admin only.

Dog profiles will never be deleted. 

===
