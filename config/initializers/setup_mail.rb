if Rails.env.development?

  ActionMailer::Base.delivery_method = :smtp
  ActionMailer::Base.smtp_settings = {
    address:        'smtp.mailtrap.io',
    port:           '2525',
    authentication: :plain,
    user_name:      ENV['MAILTRAP_USERNAME'],
    password:       ENV['MAILTRAP_PASSWORD'],
    domain:         'smtp.mailtrap.io',
    enable_starttls_auto: true
  }
end