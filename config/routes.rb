Rails.application.routes.draw do
  

  devise_for :users
  #Compass members
  resources :members

 #Administrate dashboard
  namespace :admin do
    resources :contacts
    resources :dogs
    resources :members
    
    root to: "contacts#index"
  end

  resources :contacts


  #Adoption Pg
  get 'adoption' => 'dogs#adoption'

  #Adoptable dogs on Michigan Pg
  get 'michigan' => 'dogs#michigan'
  post 'michigan' => 'dogs#michigan'

  #Adoptable dogs on Michigan Pg
  get 'texas' => 'dogs#texas'
  post 'texas' => 'dogs#texas'

  #Adoptable dogs on Michigan Pg
  get 'foster' => 'dogs#foster'
  post 'foster' => 'dogs#foster'

  #Pending Adoption dogs 
  get 'pending' => 'dogs#pending'
  post 'pending' => 'dogs#pending'

  #Happy Tails dogs 
  get 'homed' => 'dogs#homed'
  post 'homed' => 'dogs#homed'

  resources :dogs

  get 'welcome/about'
  post 'welcome/about'

  get 'welcome/journey'

  get 'welcome/events'
  post 'welcome/events'

  get 'welcome/help'

  root to: 'welcome#index'


end
