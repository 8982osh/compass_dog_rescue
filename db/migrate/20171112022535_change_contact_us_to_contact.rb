class ChangeContactUsToContact < ActiveRecord::Migration
  def change
  	rename_table :contact_us, :contacts
  end
end
