class DropActiveAdmin < ActiveRecord::Migration
  def up
  	drop_table :active_admin_comments
  	drop_table :admin_users
  end

  def down
    fail ActiveRecord::IrreversibleMigration
  end
end
