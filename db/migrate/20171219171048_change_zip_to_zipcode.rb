class ChangeZipToZipcode < ActiveRecord::Migration
  def change
  	rename_column :members, :zip, :zipcode
  end
end
