class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.string :first_name
      t.string :last_name
      t.string :address_one
      t.string :address_two
      t.string :city
      t.string :state
      t.integer :zip
      t.string :ph_primary
      t.string :ph_secondary
      t.string :email

      t.timestamps null: false
    end
  end
end
