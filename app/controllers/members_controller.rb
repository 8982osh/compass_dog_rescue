class MembersController < ApplicationController

  def index
  	@member = Member.all
  end

  def show
  end

  def new
  	@member = Member.new
  end

  def edit
  	@member = Member.find(params[:id])
  end

  def create
  	@member = Member.new(member_params)
    if @member.save
      flash[:notice] = "Member was saved successfully."
      redirect_to @member
    else
      flash[:error] = "Error: #{@member.errors.full_messages}."
      render :new 
    end
  end

  def update
    @member = Member.find(params[:id])  
    if @member.update_attributes(member_params)
       flash[:notice] = "Update successful."
       redirect_to @member
    else
      flash[:error] = "Error saving input. Please try again."
      render :edit
    end
  end

  def destroy
  	@member = Member.find(member_params[:id])
  	if @member.destroy
      flash[:notice] = "Member has been removed."
      redirect_to @member
    else
      flash[:error] = "Error: #{@member.errors.full_messages}."
    end
  end 

  private
  def member_params
  	 params.require(:member).permit(:first_name, :last_name, :address_one, :address_two, 
  	  	:city, :state, :zipcode, :ph_primary, :ph_secondary, :email)
  end

end
