class ContactsController < ApplicationController
  
  def index
    @contacts = Contact.all
  end

  def show
    @contact = Contact.find(params[:id])
  end

  def edit
  end

  def new
  	@contact = Contact.new
  end

  def create
  	@contact = Contact.new(contact_params)
  	if @contact.save
      ContactMailer.new_contact(@contact).deliver_now
  		redirect_to @contact 
  	else
      flash[:error] = @contact.errors.full_messages
      render :new
    end
  end

  def destroy
    @contact = Contact.find(params[:contact_id])
    if @contact.destroy
      flash[:notice] = "Email was deleted."
      redirect_to @contact
    else
      flash[:error] = "Error: #{@contact.errors.full_messages}."
      render :show
    end 
  end


  private
    def contact_params
  	  params.require(:contact).permit(:first_name, :last_name, :email, :subject, :description)
    end
end

