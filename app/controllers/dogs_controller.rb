class DogsController < ApplicationController

  def index
    @dogs = Dog.all
    authorize @dogs
  end

  #Display only 12 foster dogs on index.html.erb
  def foster
    @dogs = Dog.foster.page(params[:page]).per(12)
  end

  #Display only 12 adoptable dogs on michigan.html.erb
  def michigan
    @dogs = Dog.adoptable.page(params[:page]).per(12)
  end

  #Display only dogs for Texas pg
  def texas
    @dogs = Dog.texas.page(params[:page]).per(12)
  end

  #Display only dogs on hold
  def pending
    @dogs = Dog.pending.page(params[:page]).per(12)
  end 

  #Display only dogs adopted
  def homed
    @dogs = Dog.homed.page(params[:page]).per(12)
  end
  
  def show
  	@dog = Dog.find(params[:id])
  end

  def new
  	@dog = Dog.new
    authorize @dog
  end

  def edit
  	@dog = Dog.find(params[:id])
    authorize @dog
  end

  def create
  	@dog = Dog.new(dog_params)
    authorize @dog
    if @dog.save
      flash[:notice] = "Dog was saved successfully."
      redirect_to @dog
    else
      flash[:error] = "Error: #{@dog.errors.full_messages}."
      render :new 
    end
  end

  def update
    @dog = Dog.find(params[:id]) 
    authorize @dog 
    if @dog.update_attributes(dog_params)
       flash[:notice] = "Update successful."
       redirect_to @dog
    else
      flash[:error] = "Error saving input. Please try again."
      render :edit
    end
  end

  def destroy
  	@dog = Dog.find(dog_params[:id])
    authorize @dog
  	if @dog.destroy
      flash[:notice] = "Dog has been removed."
      redirect_to @dog
    else
      flash[:error] = "Error: #{@dog.errors.full_messages}."
    end 
  end


  private
  def dog_params
    params.require(:dog).permit(:animal_id, :name, :status, :gender, :weight, :age, :breed_type, :breed_one, :breed_two,
         :cat_compatible, :dog_compatible, :human_compatible, :energy_level, :temperament, :fee, :description, :image)
  end
end

