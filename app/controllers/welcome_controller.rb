class WelcomeController < ApplicationController
  def index
  end

  def about
  end

  def journey
  end

  def events
  end

  def help
  end

end
