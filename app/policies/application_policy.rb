class ApplicationPolicy
  attr_reader :user, :dog

  def initialize(user, dog)
    @user = user
    @dog = dog
  end

  def index?
    true
  end

  def show?
    scope.where(:id => dog.id).exists?
  end

  def create?
    user.present? && (user.admin? || user.moderator?)
  end

  def new?
    create?
  end

  def update?
    create?
  end

  def edit?
    update?
  end

  def destroy?
    update?
  end

  def scope
    record.class
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope
    end
  end
end
