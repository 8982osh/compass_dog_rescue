class ApplicationMailer < ActionMailer::Base

  default from: "support@compassdogrescue.com"
  layout 'mailer'
  
end
