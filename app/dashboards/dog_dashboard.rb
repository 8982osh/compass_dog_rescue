require "administrate/base_dashboard"

class DogDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    animal_id: Field::String,
    name: Field::String,
    status: Field::Select.with_options(collection: Dog::STATUS_CHOICE), 
    gender: Field::Select.with_options(collection: Dog::GENDER_CHOICE),
    weight: Field::Number,
    age: Field::Select.with_options(collection: Dog::AGE_CHOICE),
    breed_type: Field::Select.with_options(collection: Dog::BREED_TYPE_CHOICE),
    breed_one: Field::Select.with_options(collection: Dog::BREED_CHOICE),
    breed_two: Field::Select.with_options(collection: Dog::BREED_CHOICE),
    cat_compatible: Field::Select.with_options(collection: Dog::CAT_CHOICE),
    dog_compatible: Field::Select.with_options(collection: Dog::DOG_CHOICE),
    human_compatible: Field::Select.with_options(collection: Dog::HUMAN_CHOICE),
    energy_level: Field::Select.with_options(collection: Dog::ENERGY_CHOICE),
    temperament: Field::Select.with_options(collection: Dog::TEMPERAMENT_CHOICE),
    fee: Field::Number,
    description: Field::Text,
    image: Field::Carrierwave,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :animal_id,
    :name,
    :status,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :animal_id,
    :name,
    :status, 
    :gender,
    :weight,
    :age,
    :breed_type,
    :breed_one,
    :breed_two,
    :cat_compatible,
    :dog_compatible,
    :human_compatible,
    :energy_level,
    :temperament,
    :fee,
    :description,
    :image,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :animal_id,
    :name,
    :status, 
    :gender,
    :weight,
    :age,
    :breed_type,
    :breed_one,
    :breed_two,
    :cat_compatible,
    :dog_compatible,
    :human_compatible,
    :energy_level,
    :temperament,
    :fee,
    :description,
    :image,
  ].freeze

  # Overwrite this method to customize how dogs are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(dog)
  #   "Dog ##{dog.id}"
  # end
end
