module FormValidation
   extend ActiveSupport::Concern

   #Duplicate validations used for both Members Form and Contact Us Form
   included do

     before_save { self.email = email.downcase }
     before_save :capital_names
     validates :first_name, length: { maximum: 30 }, presence: true
     validates :last_name, length: { maximum: 30 }, presence: true
     VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
     validates :email, presence: true, length: { maximum: 100 }, 
               format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
   end

  private 
  #Capitalize input entered on all forms
  def capital_names 
    self.first_name = first_name.split(' ').each { |a| a.capitalize! }.join(' ')
    self.last_name = last_name.split(' ').each {|a| a.capitalize! }.join(' ')
    self.address_one = address_one.split(' ').each {|a| a.capitalize! }.join(' ')
    self.city = city.split(' ').each {|a| a.capitalize! }.join(' ')   
  end
end