class Contact < ActiveRecord::Base
	
  include FormValidation
  
  validates :subject, presence: true
  validates :description, presence: true

end

