class Member < ActiveRecord::Base
  
  #Validates first_name, last_name, email, capitalization
  include FormValidation

  before_validation :address_two_has_a_value
  
  validates :address_one, presence: true
  validates :address_two, presence: true, if: :address_two_has_a_value, allow_blank: true
  validates :city, presence: true
  validates :state, presence: true
  validates :zipcode, numericality: { only_integer: true }, presence: true
  VALID_PHONE_REGEX = /\A\d{3}-\d{3}-\d{4}\z/
  validates :ph_primary, format: { with: VALID_PHONE_REGEX }, presence: true
  validates :ph_secondary, format: { with: VALID_PHONE_REGEX }, allow_blank: true
            
  private 
  #Capitalize only if data entered in address_two 
  def address_two_has_a_value
    self.address_two = address_two.split(' ').map {|a| a.capitalize }.join(' ')
  end

  attr_accessor :state_choice

  STATE_CHOICE = ['Texas', 'Michigan']
end
